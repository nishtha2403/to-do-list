const mysql = require('mysql');

database = () => {
    let pool  = mysql.createPool({
        connectionLimit : 10,
        host            : process.env.HOST,
        user            : process.env.DATABASE_USER,
        password        : process.env.PASSWORD,
        database        : process.env.DATABASE
    });
    
    return pool;
};

module.exports = database;