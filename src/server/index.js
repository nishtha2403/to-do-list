const express = require('express');
const dotenv = require('dotenv');
const userRouter = require('./routes/user');
const tasksRouter = require('./routes/tasks');
const subtasksRouter = require('./routes/subtasks');

const app = express();
dotenv.config();
const PORT = process.env.PORT || 3000;

//middleware
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));


app.use('/user', userRouter);
app.use('/tasks', tasksRouter);
app.use('/subtasks', subtasksRouter);

app.listen(PORT, () => {
    console.log(`Server started on ${PORT}`);
});