const express = require('express');
const authenticateJWT = require('../auth');
const database = require('../database');

const pool = database();

const router = express.Router();

// Subtasks of all the users
router.get('/', authenticateJWT, (req,res) => {
    pool.getConnection((err,conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            let sql = `SELECT * FROM subtasks`;
            conn.query(sql,(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    res.json(result);
                }
                conn.release();
            });
        }
    });
});

// All subtasks of a specific user whose id is passed in params
router.get('/userid/:id', authenticateJWT, (req,res) => {
    pool.getConnection((err,conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const id = req.params.id;
            let sql = `SELECT subtasks.id,subtasks.title, subtasks.description, subtasks.is_complete
            FROM subtasks
            INNER JOIN tasks
            ON subtasks.task_id = tasks.id
            INNER JOIN users
            ON tasks.user_id = users.id
            WHERE users.id = ?`;

            conn.query(sql,[id],(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    res.json(result);
                }
                conn.release();
            });
        }
    });
});

// Subtask of specific task id
// Subtasks of the user and task id is passed in query params
router.get('/taskid', authenticateJWT, (req,res) => {
    pool.getConnection((err,conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const userid = req.query.userid;
            const taskid = req.query.taskid;
            sql = `SELECT subtasks.id,subtasks.title,subtasks.description,subtasks.is_complete FROM subtasks
            INNER JOIN tasks
            ON subtasks.task_id = tasks.id
            INNER JOIN users 
            ON tasks.user_id = users.id
            WHERE (tasks.user_id = ? AND subtasks.task_id = ?)`;
            conn.query(sql,[userid,taskid],(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    res.json(result);
                }
                conn.release();
            });
        }
    });
});

// Create new subtask for taskid passed in query params for the logged in user
router.post('/newsubtask', authenticateJWT, (req,res) => {
    pool.getConnection((err,conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const taskid = req.query.taskid;
            const {email} = req.user;
            let sql = `SELECT id FROM users WHERE email='${email}'`;
            console.log(req.user);
            conn.query(sql,(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    const userid = result[0].id;
                    let sql = `SELECT COUNT(*) FROM tasks WHERE id = ? AND user_id= ?`;
                    conn.query(sql,[taskid,userid],(err,result) => {
                        if(err) {
                            console.error(err);
                        } else {
                            const count = result[0]['COUNT(*)'];
                            if(count === 1){
                                let subtask = [Object.values(req.body)];
                                subtask[0].unshift(taskid);
                                sql = `INSERT INTO subtasks (task_id,title,description) VALUES ?`;
                                conn.query(sql,[subtask],(err,result) => {
                                    if(err) {
                                        console.error(err);
                                    } else {
                                        res.json({msg: 'Subtask added!'});
                                    }
                                    conn.release();
                                });
                            } else {
                                res.status(400).json({msg: `Unauthorized user or Task doesn't exists`});
                                conn.release();
                            }
                        }
                    });
                }
            });
        }
    });
});

//Update Task
router.put('/:subtaskid', authenticateJWT, (req,res) => {
    pool.getConnection((err,conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            if(JSON.stringify(req.body) === '{}'){
                return res.status(400).json({msg: 'Please give updates'});
            } else {
                const subtaskid = req.params.subtaskid;
                const updates = req.body;
                const {email} = req.user;
                
                let sql = `SELECT id FROM users WHERE email = '${email}'`;
                conn.query(sql,(err,result) => {
                    const userid = result[0].id;
                    sql = `SELECT COUNT(*) FROM subtasks 
                    INNER JOIN tasks
                    ON subtasks.task_id = tasks.id
                    INNER JOIN users
                    ON tasks.user_id = users.id
                    WHERE subtasks.id= ? AND  tasks.user_id= ?`;
                    conn.query(sql,[subtaskid,userid],(err,result) => {
                        const count = result[0]['COUNT(*)'];
                        if(count == 0) {
                            return res.status(401).json({msg:'Unauthorized user or subtask doesnt exists'});
                        } else {
                            sql = `UPDATE subtasks SET ? WHERE subtasks.id= ?`;
                            conn.query(sql,[updates , subtaskid],(err,result) => {
                                if(err) {
                                    console.error(err);
                                } else {
                                    res.json({msg: 'Task Updated!'});
                                }
                                conn.release();
                            });
                        }
                    });
                });
            }
        }
    });
});

// Delete subtask whose id is present in params and subtask belong to logged in user
router.delete('/:subtaskid', authenticateJWT, (req,res) => {
    pool.getConnection((err,conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const subtaskid = req.params.subtaskid;
            const {email} = req.user;

            let sql = `SELECT id FROM users WHERE email = '${email}'`;

            conn.query(sql,(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    const userid = result[0].id;
                    sql = `SELECT COUNT(*) FROM subtasks 
                    INNER JOIN tasks
                    ON subtasks.task_id = tasks.id
                    INNER JOIN users
                    ON tasks.user_id = users.id
                    WHERE subtasks.id= ? AND  tasks.user_id= ?`;
                    conn.query(sql,[subtaskid,userid],(err,result) => {
                        const count = result[0]['COUNT(*)'];
                        if(count == 0) {
                            return res.status(401).json({msg:'Unauthorized user or subtask doesnt exists'});
                        } else {
                            sql = `DELETE FROM subtasks WHERE id= ?`;
                            conn.query(sql,[subtaskid],(err) => {
                                if(err) {
                                    console.error(err);
                                    res.sendStatus(400);
                                } else {
                                    res.json({msg: 'Task Deleted'});
                                }
                                conn.release();
                            });
                        }
                    });
                }
            });
        }
    });
});


module.exports = router;