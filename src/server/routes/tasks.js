const express = require('express');
const authenticateJWT = require('../auth');
const database = require('../database');

const pool = database();

const router = express.Router();

// Tasks of all the users 
router.get('/', authenticateJWT, (req,res) => {
    pool.getConnection((err, conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            let sql = `SELECT * FROM tasks`;
            conn.query(sql,(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    res.json(result);
                }
                conn.release();
            });
        }
    });
});

// Tasks of user whose id is passed in params
router.get('/:id', authenticateJWT, (req,res) => {
    pool.getConnection((err, conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const id = req.params.id;
            let sql = `SELECT tasks.id,title,description,is_complete
            FROM tasks
            INNER JOIN users
            ON tasks.user_id = users.id
            WHERE users.id = ?`;
            conn.query(sql,[id],(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    res.json(result);
                }
                conn.release();
            });
        }
    });
});

// Create new task
router.post('/newtask', authenticateJWT, (req,res) => {
    pool.getConnection((err, conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const {email} = req.user;
            let sql = `SELECT id FROM users WHERE email = '${email}'`;
            conn.query(sql,(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    const userid = result[0].id;
                    let task = [Object.values(req.body)];
                    task[0].unshift(userid);
                    sql = `INSERT INTO tasks (user_id,title,description) VALUES ?`;
                    conn.query(sql,[task],(err,result) => {
                        if(err) {
                            console.error(err);
                        } else {
                            res.json({msg:'Task Added!'});
                        }
                        conn.release();
                    });
                }
            });   
        }
    });
});

// Update task
router.put('/:taskid', authenticateJWT, (req,res) => {
    pool.getConnection((err, conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            if(JSON.stringify(req.body) === '{}'){
                return res.status(400).json({msg: 'Please give updates'});
            } else {
                const taskid = req.params.taskid;
                const updates = req.body;
                const {email} = req.user;
                
                let sql = `SELECT id FROM users WHERE email = '${email}'`;
                conn.query(sql,(err,result) => {
                    const userid = result[0].id;
                    sql = `UPDATE tasks SET ? WHERE id = ? AND user_id = ?`;
                    conn.query(sql,[updates, taskid, userid],(err,result) => {
                        if(err) {
                            console.error(err);
                        } else {
                            if(result.affectedRows === 1){
                                res.json({msg: 'Task Updated'});
                            } else {
                                res.status(401).json({msg:'Unauthorized user'});
                            }
                        }
                        conn.release();
                    });
                });
            }
        }
    });
});

// Delete task whose id is passed in param only if the task belong to logged in user
router.delete('/:taskid', authenticateJWT, (req,res) => {
    pool.getConnection((err, conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const taskid = req.params.taskid;
            const {email} = req.user;

            let sql = `SELECT id FROM users WHERE email = '${email}'`;

            conn.query(sql,(err,result) => {
                if(err) {
                    console.error(err);
                } else {
                    const userid = result[0].id;
                    sql = `SELECT COUNT(*) FROM tasks WHERE id= ? AND user_id= ?`;
                    conn.query(sql,[taskid,userid],(err,result) => {
                        const count = result[0]['COUNT(*)'];
                        if(count == 0) {
                            return res.status(401).json({msg:'Unauthorized user or Task is already deleted'});
                        } else {
                            sql = `DELETE FROM subtasks WHERE task_id= ?`;
                            conn.query(sql,[taskid],(err) => {
                                if(err) {
                                    console.error(err);
                                    res.sendStatus(400);
                                } else {
                                    sql = `DELETE FROM tasks WHERE id=? AND user_id= ?`;    
                                    conn.query(sql,[taskid,userid],(err) => {
                                        if(err) {
                                            console.error(err);
                                        } else {
                                            res.json({msg: 'Task Deleted!'});
                                        }
                                        conn.release();
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

module.exports = router;