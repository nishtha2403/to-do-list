const express = require('express');
const jwt = require('jsonwebtoken');
const encrypt = require('../security/encrypt');
const comparePassword = require('../security/comparePass');
const authenticateJWT = require('../auth');
const database = require('../database');

const pool = database();

const router = express.Router();

router.post('/signup',(req,res) => {
    pool.getConnection((err,conn) => {
        if(err) {
            res.status(500).json(err);
        } else {
            if(req.body === undefined) {
                res.status(400).json({msg: "Please give user details"});
            } else {
                const {email} = req.body;
                let sql = `SELECT COUNT(*) FROM users WHERE email = ?`;
                conn.query(sql,email,(err,result) => {
                    const count = result[0]['COUNT(*)'];
                    if(count === 1){
                        res.status(400).json({msg: "user already exists"});
                    } else {
                        const {name,email,password} = req.body;
                        encryptedPass = encrypt(password);
                        const userDetails = {
                            name,
                            email,
                            ['password']:encryptedPass
                        };
                        sql = `INSERT INTO users SET ?`;
                        conn.query(sql, userDetails, (err,result) => {
                            if(err) {
                                console.error(err);
                            } else {
                                res.json({msg:'User Created'});
                            }
                            conn.release();
                        });
                    }
                });
            }
        }
    });
});

router.post('/login', (req,res) => {
    pool.getConnection((err, conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            const email = req.body.email;
            const password = req.body.password;

            let sql = `SELECT * FROM users WHERE email = ? `;
            conn.query(sql,[email],(err,result) => {
                if(err) {
                    console.error(err);
                    res.status(403).json({msg: `username is incorrect`});
                } else {
                    const isPasswordCorrect = comparePassword(password,result[0].password);
                    if(isPasswordCorrect){
                    //user verified now generate token for him
                    const secretkey = process.env.TOKEN_SECRET;
                    const refreshSecretKey = process.env.REFRESH_TOKEN_SECRET;

                    const accessToken = jwt.sign({email}, secretkey, {expiresIn: '2d'});
                    const refreshToken = jwt.sign({email},refreshSecretKey);

                    res.json({
                        accessToken,
                        refreshToken
                    });
                    } else {
                        res.status(403).json({msg: `password is incorrect`});
                    }
                }
            });
        }
    });
});

router.post('/refreshtoken', (req,res) => {
    const { refreshToken } = req.body;
    const secretkey = process.env.TOKEN_SECRET;
    const refreshSecretKey = process.env.REFRESH_TOKEN_SECRET;

    if(!refreshToken) {
        return res.sendStatus(401);
    }

    jwt.verify(refreshToken, refreshSecretKey, (err,user) => {
        if(err) {
            return res.sendStatus(403);
        }

        const accessToken = jwt.sign(user, secretkey, {expiresIn: '2d'});

        res.json({
            accessToken
        });
    })

});

router.get('/', authenticateJWT, (req,res) => {
    pool.getConnection((err, conn) => {
        if(err) {
            res.sendStatus(500);
        } else {
            let sql = `SELECT * FROM users`;
            conn.query(sql,(err,result) => {
                if(err){
                    console.error(err);
                } else {
                    res.json(result);
                }
                conn.release();
            });
        }
    });
});

module.exports = router;