const bcrypt = require('bcrypt');
const saltrounds = 10;

const comparePassword = async (password,dbPassword) => {
     const isPasswordCorrect = await bcrypt.compare(password,dbPassword);
     return isPasswordCorrect;
};

module.exports = comparePassword;