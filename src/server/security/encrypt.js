const bcrypt = require('bcrypt');
const saltrounds = 10;

const encrypt = async (password) => {
     const encryptedPass = await bcrypt.hash(password, saltrounds);
     return encryptedPass;
};

module.exports = encrypt;